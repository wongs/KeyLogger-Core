﻿namespace KeyLogger_Core.Mail
{
    public class MailHostConfig
    {
        public string FromAddress { get; set; }
        public string Password { get; set; }
        public string ToMails { get; set; }
        public string Server { get; set; }
        public bool UseDefaultCredentials { get; set; }
        public bool EnableSsl { get; set; }
        public int Port { get; set; }
        public string MailDisplayName { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
    }
}
